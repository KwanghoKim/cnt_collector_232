import sys, os, time, json
import serial
import paho.mqtt.client as mqtt
import argparse

if os.system("lsmod | grep max7301") == 0:
    os.system("rmmod gpio-max7301")
    os.system("rmmod gpio-max730x")
    os.system("rmmod gpio-pisosr")
    os.system("insmod /lib/modules/5.4.31/extra/max3109test.ko")

serial_MFC = serial.Serial('/dev/ttyMAX0', baudrate=115200, bytesize=8,
                           parity='N', stopbits=1, timeout=0.5)

serial_TEMP = serial.Serial('/dev/ttyMAX3', baudrate=9600, bytesize=8,
                           parity='N', stopbits=1, timeout=0.5)

serial_WINDER = serial.Serial('/dev/ttyMAX2', baudrate=9600, bytesize=8,
                           parity='N', stopbits=1, timeout=0.5)

parser = argparse.ArgumentParser()
parser.add_argument('-config',help='')
args = parser.parse_args()

ROOT_PATH = f'/usr/local/sdt/app/{args.config}'

with open(f'{ROOT_PATH}/connect_info.json') as f:
    info = json.load(f)

MQTT_TOPIC = info['mqtt']['topic']
# MQTT_TOPIC = "/axrcnt/device-test/S0NQ0R223090003"
MQTT_ID = info['mqtt']['id']
MQTT_PW = info['mqtt']['pw']
MQTT_HOST_IP = info['mqtt']['host_ip']
MQTT_PORT = info['mqtt']['port']

mqtt_client = mqtt.Client(info['mqtt']['assetCode'])
mqtt_client.username_pw_set(MQTT_ID, MQTT_PW)

def MFC_Read_Flow(client):
    command_hex = '235246310D' # '#RF1'
    command_bytes = bytes.fromhex(command_hex)

    client.write(command_bytes)
    time.sleep(0.02)
    ch1 = client.readline()
    # print(ch1)
    ch1 = str(ch1, 'utf-8')
    # print(ch1)
    if ch1 != '':        
        ch1 = float(ch1[:-1])
    else:
        ch1 = None
    time.sleep(0.02)

    command_hex = '235246320D' # '#RF2'
    command_bytes = bytes.fromhex(command_hex)

    client.write(command_bytes)
    time.sleep(0.02)
    ch2 = client.readline()
    # print(ch2)
    ch2 = str(ch2, 'utf-8')
    # print(ch2)
    if ch2 != '':
        ch2 = float(ch2[:-1])
    else:
        ch2 = None
    
    time.sleep(0.02)

    data = {
        "assetCode": info['mqtt']['assetCode'],
        "timestamp": int(time.time()),
        "dataType": "DATA",
        "data": {
        }
    }

    if ch1 != None:
        data['data']['hydrogen1'] = ch1
    
    if ch2 != None:
        data['data']['hydrogen2'] = ch2

    # print(f'MFC: {data}')
    Publish_mqtt(data)

def MFC_Write(client, cmd, ch, subcmd=None, value=None):
    send_data = ['#', 'S']
    if cmd == 'SetPoint':
        send_data.append('S')
    elif cmd == 'SetFlow':
        send_data.append('F')
    
    if ch == 'hydrogen1':
        send_data.append('1')
    else: # ch == 'hydrogen2
        send_data.append('2')

    if subcmd != None:
        send_data.append(' ')
        if subcmd == 'On':
            send_data.append('1')
        else: # subcmd = 'Off'
            send_data.append('0')
    
    if value != None:
        send_data.append(' ')
        send_data.append(value)

    send_data.append('\r')
    send_data = ''.join(send_data)
    send_data = send_data.encode()

    send_data_hex = send_data.hex()
    send_data_hex = bytes.fromhex(send_data_hex)

    client.write(send_data_hex)
    time.sleep(0.02)

    result = client.readline()

def Temp_Hum_Press_Read(client):
    ch1_temp, ch1_humi, ch1_pres, ch2_temp, ch2_humi = None, None, None, None, None
    command_hex = '410030305A53300D'
    command_bytes = bytes.fromhex(command_hex)
    
    client.write(command_bytes)
    time.sleep(0.3)
    ch1 = client.readline()
    #time.sleep(0.02)  
    # print(f'Receive: 00 {ch1}')
    parts = ch1.split()
    if len(parts) > 7:
        if b'\xf8C' in parts[2]:
            ch1_temp = float(parts[2][:-2])
        else:
            ch1_temp = None
        
        if b'RH%' in parts[3]:
            ch1_humi = float(parts[3][:-3])
        else:
            ch1_humi = None
        
        if b'hPa' in parts[7]:
            ch1_pres = float(parts[7][:-3])
    
    #time.sleep(5)

    command_hex = '410030315A53300D'
    command_bytes = bytes.fromhex(command_hex)

    client.write(command_bytes)
    time.sleep(0.3)
    ch2 = client.readline()
    #time.sleep(0.02)
    # print(f'Receive: 01 {ch2}')
    parts = ch2.split()

    if len(parts) > 7:
        if b'\xf8C' in parts[2]:
            ch2_temp = float(parts[2][:-2])
        else:
            ch2_temp = None
        
        if b'RH%' in parts[3]:
            ch2_humi = float(parts[3][:-3])
        else:
            ch2_humi = None

    data = {
        "assetCode":info['mqtt']['assetCode'],
        "timestamp": int(time.time()),
        "dataType": "DATA",
        "data": {
        }
    }
    if ch1_temp != None:
        data['data']['tempOn'] = ch1_temp
    
    if ch1_humi != None:
        data['data']['humiOn'] = ch1_temp
    
    if ch1_pres != None:
        data['data']['presOn'] = ch1_pres
    
    if ch2_temp != None:
        data['data']['tempSb'] = ch2_temp
    
    if ch2_humi != None:
        data['data']['humiSb'] = ch2_humi

    # print(data)

    Publish_mqtt(data)

def Set_WinderSpeed(client, value):
    return 0

def Publish_mqtt(data):
    send_mqtt_data = json.dumps(data, indent=4)

    while True:
        try:
            mqtt_client.connect(host=MQTT_HOST_IP, port=MQTT_PORT)
            break
        except:
            continue
        
    mqtt_client.publish(topic=MQTT_TOPIC, payload=send_mqtt_data)

    while True:
        try:
            mqtt_client.disconnect()
            break
        except:
            continue

def Command_Read():
    with open(f'{ROOT_PATH}/control.json', 'r', encoding='UTF-8') as f:
        cmd = json.load(f)
    
    # print(cmd)
    if cmd['type'] != 'null':
        if 'start' in cmd['type']:
            if cmd['cmd']['hydrogen1'] != 'null':
                MFC_Write(client=serial_MFC, ch='hydrogen1', cmd='SetPoint', value=cmd['cmd']['hydrogen1'])
                MFC_Write(client=serial_MFC, ch='hydrogen1', cmd='SetFlow', subcmd='On')
                
            if cmd['cmd']['hydrogen2'] != 'null':
                MFC_Write(client=serial_MFC, ch='hydrogen2', cmd='SetPoint', value=cmd['cmd']['hydrogen2'])
                MFC_Write(client=serial_MFC, ch='hydrogen2', cmd='SetFlow', subcmd='On')
            
            if cmd['cmd']['speedRol'] != 'null':
                Set_WinderSpeed(serial_WINDER, value=cmd['cmd']['speedRol'])
        
        elif 'change' in cmd['type']:
            if cmd['cmd']['hydrogen1'] != 'null':
                MFC_Write(client=serial_MFC, ch='hydrogen1', cmd='SetPoint', value=cmd['cmd']['hydrogen1'])
            
            if cmd['cmd']['hydrogen2'] != 'null':
                MFC_Write(client=serial_MFC, ch='hydrogen2', cmd='SetPoint', value=cmd['cmd']['hydrogen2'])

            if cmd['cmd']['speedRol'] != 'null':
                Set_WinderSpeed(serial_WINDER, value=cmd['cmd']['speedRol'])

        else: # 'stop' in cmd['type]
            MFC_Write(client=serial_MFC, ch='hydrogen1', cmd='SetFlow', subcmd='Off')
            MFC_Write(client=serial_MFC, ch='hydrogen2', cmd='SetFlow', subcmd='Off')
            Set_WinderSpeed(serial_WINDER, value=cmd['cmd']['speedRol'])

        cmd = {
            'type': 'null',
            'cmd': {
                'hydrogen1': 'null',
                'hydrogen2': 'null',
                'speedRol': 'null'
            } 
        }
        with open(f'{ROOT_PATH}/control.json', 'w') as f:
            json.dump(cmd, f)

while True:
    start = time.time()

    Command_Read()
    
    MFC_Read_Flow(serial_MFC)

    Temp_Hum_Press_Read(serial_TEMP)

    end = time.time()
    diff = end - start
    # print(diff)
    if diff < 3:
        time.sleep(3 - diff)